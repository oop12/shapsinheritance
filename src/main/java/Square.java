/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Square extends Rectangle {

    public Square(double side) {
        super(side, side);
        System.out.println("Square created");
    }

    @Override
    public double calArea() {
        if (width > 0 && height > 0) {
            double area = width * height;
            return area;
        }
        Error();
        return 0;
    }

    @Override
    public void print() {
        System.out.println("Area of Square(" + this.width
                + "," + this.height + ") = " + calArea());
    }

    private void Error() {
        System.out.println("Area of Square -> Error : Side must more than zero!!!");
    }

}
