/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Triangle extends Shape {

    private double base;
    private double height;

    public Triangle(double base, double height) {
        super();
        this.base = base;
        this.height = height;
        System.out.println("Triangle created");
    }

    @Override
    public double calArea() {
        if (base > 0 && height > 0) {
            double area = 0.5 * base * height;
            return area;
        }
        Error();
        return 0;
    }

    @Override
    public void print() {
        System.out.println("Area of Triangle(" + this.base
                + "," + this.height + ") = " + calArea());
    }

    private void Error() {
        System.out.println("Area of Triangle -> "
                + "Error : Hight and Base must more than zero!!!");
    }

}
