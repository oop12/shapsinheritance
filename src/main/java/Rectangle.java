/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Rectangle extends Shape {

    protected double width;
    protected double height;

    public Rectangle(double width, double height) {
        super();
        this.width = width;
        this.height = height;
        System.out.println("Rectangle created");
    }

    @Override
    public double calArea() {
        if (width > 0 && height > 0) {
            double area = width * height;
            return area;
        }
        Error();
        return 0;
    }

    @Override
    public void print() {
        System.out.println("Area of Rectangle(" + this.width
                + "," + this.height + ") = " + calArea());
    }

    private void Error() {
        System.out.println("Area of Rectangle -> "
                + "Error : Hight and Width must more than zero!!!");
    }

}
