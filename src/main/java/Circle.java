/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Circle extends Shape {

    private double r;
    private double pi = 22.0 / 7;

    public Circle(double r) {
        super();
        this.r = r;
        System.out.println("Circle created");
    }

    @Override
    public double calArea() {
        if (this.r > 0) {
            double area = pi * r * r;
            return area;
        }
        Error();
        return 0;
    }

    @Override
    public void print() {
        System.out.println("Area of Circle(" + this.r + ") = " + calArea());
    }

    private void Error() {
        System.out.println("Area of Circle -> Error : Radius must more than zero!!!");
    }
}
