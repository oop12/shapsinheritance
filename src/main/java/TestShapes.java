/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestShapes {

    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.print();
        line();

        Circle cir1 = new Circle(3);
        cir1.print();
        line();

        Circle cir2 = new Circle(4);
        cir2.print();
        line();

        Triangle tri = new Triangle(4, 3);
        tri.print();
        line();

        Rectangle rec = new Rectangle(4, 3);
        rec.print();
        line();

        Square squ = new Square(2);
        squ.print();
        line();
        
        System.out.println("+--------Polymorphism--------+");
        Shape[] shapes ={cir1,cir2,tri,rec,squ};
        for(int i=0; i<shapes.length; i++){
            shapes[i].print();
            line();
        }
        

    }

    private static void line() {
        System.out.println("-------------");
    }
}
